package model;

import java.util.List;
import java.util.Map;

import test.DBUtil;

public class UserService {
	private DBUtil db;

	public UserService() {
		db=new DBUtil();
	}
	/**
	 * 判断用户名
	 * @param username
	 * @return true(用户名合法可用) false(用户名已被使用)
	 */
	public boolean check(String username) {
		boolean f=false;
		String sql="select * from user where username=?";
		Map<String, String> u=db.getMap(sql,new String [] {username});
		if (u==null) {
			f=true;
		}
		return f;
	}
	public int register(String un,String pw,String tel,String addr) {
		String sql="insert into user values(null,?,?,0,?,?)";
		String [] params= {un,pw,tel,addr};
		return db.update(sql, params);
	}
	public Map<String,String> login(String un,String pw) {
		String sql="select * from user where username=? and password=?";
		String[] params= {un,pw};
		return db.getMap(sql, params);
	}
	public int updateUser(String pw,String tel,String addr,String id) {
		String  sql="update  user set password=?,telephone=?,address=? where id=?";
		String [] params= {pw,tel,addr,id};
		return db.update(sql, params);
	}
	public int  updateuser(Map<String, String> m) {
		String sql = "update user set password=?,telephone=?,address=?, where id=?";
		String [] params = {m.get("username"),m.get("password"),m.get("telephone"),m.get("address"),m.get("id")};
		return db.update(sql,params);
	}
	public Map<String, String> getUserById(String id){
		String sql="select * from user where id=?";
		String [] params= {id};
		return db.getMap(sql,params);
	}
	public Map<String, String> gettypenameById(String id){
		String sql="select * from foodtype where id=?";
		String [] params= {id};
		return db.getMap(sql,params);
	}
	/**
	 * 根据用户名模糊查找所有用户
	 * @param username
	 * @return
	 */
	public List<Map<String, String>> getUsers(String username){
		String sql="select * from user";
		String [] params=null;
		//like模糊查找
		if (username!=null && !"".equals(username)) {
			sql=sql+" where username like ?";
			params=new String [] {"%"+username+"%"};
		}
		return db.getList(sql, params);
	}
	//删除用户
	public int deIUser(int id) {
		String sql ="delete from user where id="+id;
		return db.update(sql);
	}
}