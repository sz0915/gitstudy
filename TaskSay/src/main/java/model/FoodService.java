package model;

import java.util.List;
import java.util.Map;

import test.DBUtil;

public class FoodService {
	private DBUtil db;

	public  FoodService() {
		db=new DBUtil();
	}
	//获取热门菜
	public List<Map<String, String>> getHotFoods(){
		//String sql="select food.id,food.foodname,food.price,food.picture from order by food.hits Desc limit 0,3";
		String sql = "SELECT food.id,food.foodname,food.price,food.picture FROM food ORDER BY food.hits DESC LIMIT 0, 3";
		return db.getList(sql);
	}
	//获取特价菜
	public List<Map<String, String>> getSpecialFoods(){
		//String sql="select id,foodname,price,picture from food where comment>0 order by hits Desc limit 0,3";
		String sql = "SELECT food.id,food.foodname,food.price,food.picture,food.`comment` FROM food WHERE food.`comment` > 0 ORDER BY food.`comment` DESC LIMIT 0, 3";
		
		return db.getList(sql);
	}
	//获取厨师推荐
	public List<Map<String, String>> getRecommFoods(){
		String sql = "SELECT food.id,food.foodname,food.price,food.picture,food.`comment` FROM food WHERE food.`comment` = 0 ORDER BY food.price DESC LIMIT 0, 3";
		return db.getList(sql);
	}
	public Map<String, String> getFood(String id){
		String sql="SELECT f.*,ft.typename FROM food AS f JOIN foodtype AS ft ON f.type=ft.id WHERE f.id=?";
		String [] params= {id};
		return db.getMap(sql,params);
	}
	public List<Map<String,String>> getFoods(String s_fn,String s_type){
		String sql="SELECT f.*, ft.typename FROM food  f join foodtype  ft on f.type=ft.id where 1=1";
		String[] params=null;
		if(s_fn!=null&&s_type!=null) { 
			sql+=" and foodname like ? and type like ?";
		    params=new String[] {"%"+s_fn+"%","%"+s_type+"%"};}
		else if(s_fn==null&&s_type!=null){ 
			sql+=" and type like ?";
		    params=new String[] {"%"+s_type+"%"};}
		else if(s_fn!=null&&s_type==null){ 
			sql+=" and foodname like ?";
		    params=new String[] {"%"+s_fn+"%"};}
		sql+=" order by hits desc,type asc,f.id asc";
		return db.getList(sql, params);
	}
	// 修改菜品信息
		public int editFood(String foodname, String feature, String material, String price, String type, String picture,
				String comment, String id) {
			String sql = "update food set foodName=?,feature=?,material=?,price=?,type=?";
			// 如果重新上传的图片需要修改图片储存路径
			if (picture != null) {
				sql = sql + ",picture=?";
			}
			sql = sql + ",comment=? where id=?";
			if (picture != null) {
				return db.update(sql, new String[] { foodname, feature, material, price, type, picture, comment, id });

			} else {
				return db.update(sql, new String[] { foodname, feature, material, price, type, comment, id });
			}
		}
		
		//增加菜品
		public int addFood(String fn,String fea,String mat,String price,String type,String picture,String comment) {
			   String sql="insert into food values(null,?,?,?,?,?,?,0,?)";
			 	   return db.update(sql, new String[] {fn,fea,mat,price,type,picture,comment});
			  
		   }
		
		public int delFood(String id) {
			   String sql="delete from food where id=?";
			   return db.update(sql,new String[] {id});
		   }
	/*
	public static void main(String[] args) {
		FoodService fs=new FoodService();
		Map<String, String> m=fs.getFoodById("4");
		System.out.println(m);
	}
	*/
}
