package model;

import java.util.List;
import java.util.Map;
import test.DBUtil;

public class FoodTypeService {
	private DBUtil db=null;
	
	public  FoodTypeService() {
		db=new DBUtil();
	}
	public List<Map<String, String>> getAllTypes(){
		String sql = "select * from foodtype";
		return db.getList(sql);
	}
	public int delFood(String id) {
		   String sql="delete from user where id=?";
		   return db.update(sql,new String[] {id});
	   }
	public int addFood(String typename) {
		   String sql="insert into foodtype values(null,?)";
		 	   return db.update(sql, new String[] {typename});
	   }
	public int delFoods(String id) {
		   String sql="delete from foodtype where id=?";
		   return db.update(sql,new String[] {id});
	   }
	public int editFood(String typename,String id) {
		String sql = "update foodtype set typename=? where id=?";
		 return db.update(sql, new String[] {typename,id});
	}
}
