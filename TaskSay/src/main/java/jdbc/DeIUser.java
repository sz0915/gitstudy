package jdbc;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.UserService;

/**
 * Servlet implementation class DeIUser
 */
@WebServlet("/deluser")
public class DeIUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeIUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=utf-8");//输出中文编码
		PrintWriter out =response.getWriter();//获取out对象
		String idstr=request.getParameter("ID");//获取用户列表页面中要删除的用户id
		int id=Integer.parseInt(idstr);
		//调用业务层的删除用户方法
		UserService us=new UserService();
		int r= us.deIUser(id);
		//操作结果是否正常
		if (r==1) out.print("删除用户成功");
		else out.print("删除用户失败");
		out.print("<a href=pages/userList.jsp>返回删除列表</a>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
