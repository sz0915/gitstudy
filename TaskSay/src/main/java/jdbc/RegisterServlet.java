package jdbc;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "register", value = "/register")
public class RegisterServlet extends HttpServlet {
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        request.setCharacterEncoding("utf-8");
        String un = request.getParameter("un");
        String pw = request.getParameter("pw");
        String tel = request.getParameter("tel");
        String addr = request.getParameter("addr");
        if (un!=null && pw!=null && tel!=null && addr!=null) {
            /*
            1.查询用户名是否重复
            2.如果用户名不存在 就先注册
             */
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                //定义数据库的连接地址
                String url = "jdbc:mysql://localhost:3306/meal?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2B8&useSSL=false";
                conn = DriverManager.getConnection(url, "root", "root");
                String sql = "select * from user where username=?";
                pstmt = conn.prepareStatement(sql);
                //设置问好所对应的参数值
                pstmt.setString(1, un);
                //执行命令
                rs = pstmt.executeQuery();
                //判断是否存在
                if (rs.next()) {
                    out.print("用户名已存在!<br>");
                    out.print("<a href='pages/homepage.html'>返回注册页面</a>");
                    return;
                }else {
                    //用户名不重复
                    //可以注册用户
                    //插入数据库
                    String sql2 = "insert into user values(null,?,?,0,?,?)";
                    //执行命令
                    pstmt = conn.prepareStatement(sql2);
                    //设置问好所对应的参数值
                    pstmt.setString(1, un);
                    pstmt.setString(2, pw);
                    pstmt.setString(3, tel);
                    pstmt.setString(4, addr);
                    //执行插入命令
                    int n = pstmt.executeUpdate();
                    if (n==1) out.print("注册成功!<br>");
                    else out.print("注册失败!<br>");
                    out.print("<a href='pages/homepage.html'>返回注册页面</a>");

                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }finally {
                try {
                    if (rs!=null) rs.close();
                    if (pstmt!=null) pstmt.close();
                    if (conn!=null) conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }else {
            out.print("请输入完整的注册信息<a href='pages/homepages.html'>返回注册页面</a>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
    }
}
