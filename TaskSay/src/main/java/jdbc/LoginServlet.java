package jdbc;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

//@WebServlet(name = "login", value = "/login")
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    Connection conn=null;
    PreparedStatement pstmt=null;
    ResultSet rs = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        request.setCharacterEncoding("UTF-8");
        String un = request.getParameter("un");
        String pw = request.getParameter("pw");
//        out.print("dawdad");
        if (un != null && pw != null && !"".equals(un) && !"".equals(pw)) {
            try {

                Class.forName("com.mysql.cj.jdbc.Driver");
//            定义数据库的连接地址
                String url = "jdbc:mysql://localhost:3306/meal?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2B8&useSSL=false";
                conn = DriverManager.getConnection(url, "root", "root");
                String sql = "select * from user where username = '" + un + "' and password = '" + pw + "'";
//                String sql = "select * from user";
                pstmt = conn.prepareStatement(sql);
                rs =  pstmt.executeQuery();
                while (rs.next()) {
                    //登录成功, 讲登录用户信息保存到session中
                    String ident = rs.getString("ident");
                    session.setAttribute("loginID", rs.getInt(1));
                    session.setAttribute("loginName", rs.getString("username"));
                    session.setAttribute("ident", ident);
                    if (ident.equals("1")) out.print("管理员登录成功");
                    else out.print("普通用户登录成功");
//                    out.print(rs.getString("password"));
                }



            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                //关闭连接
                try {
                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (pstmt != null) {
                    try {
                        pstmt.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
    }
}
