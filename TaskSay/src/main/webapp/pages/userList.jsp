<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="model.UserService,java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<%
request.setCharacterEncoding("utf-8");
String username=request.getParameter("un");//带条件查找时输入的用户名
UserService us =new UserService();
List<Map<String, String>> users =us.getUsers(username);
%>
<body>
<div align="center">
<form action="" method="post">
<input type="text" name="un" placeholder="请输入用户名" value="<%=(username==null)?"":username%>">
<input type="submit" value="搜索">
</form>
<br><br>
<table border="1" width="70%">
<tr>
<th>序号</th><th>用户名</th><th>角色</th><th>电话</th><th>地址</th><th>操作</th>
</tr>
<tr>
<td>序号</td><td>用户名</td><td>角色</td><td>电话</td><td>地址</td><td>操作</td>
</tr>
<%
int num=0;
for(Map<String,String> u:users){
	num++;
%>
<tr>
<td><%=num %><input type="hidden" name="id" value="<%=u.get("id")%>"></td>
<td><%=u.get("username") %></td>
<td><%=u.get("ident").equals("1")?"管理员":"普通用户" %></td>
<td><%=u.get("telephone") %></td>
<td><%=u.get("address") %></td>
<td align="center">
<a href="../deluser?ID=<%=u.get("id") %>" onclick="return confirm("确定要删除吗？")">删除</a>
<a href="../pages/updateUser.jsp?ID=<%=u.get("id") %>" onclick="return confirm('确定要修改吗？')">修改</a>
</td>
</tr>
<%}
%>
</table>
</div>
</body>
</html>