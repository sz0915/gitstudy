<%@page import="model.UserService,java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/pages/head.jsp">
	<jsp:param value="管理员管理菜品类型" name="title" />
</jsp:include>
</head>
<%
String id=request.getParameter("id");
UserService us=new UserService();
Map<String,String> u= us.gettypenameById(id);
%>
<body>
	<jsp:include page="/pages/admin/admin_nav.jsp">
		<jsp:param value="food_t_m" name="param_fun" />
	</jsp:include>
	<div class="container-fluid">
		<div class="card border">
			<div class="card-header border">
				<form class="form-inline" action="admin_xiugai.action" method="post">
					<input type="hidden" name="id" value="<%=u.get("id")%>">
					<input class="form-control mr-sm-2" id="typename" value="<%=u.get("typename") %>" name="tn" type="text"
									required /> <input
						type="submit" class="btn btn-outline-success my-2 my-sm-0"
						value="修改菜品类型">
				</form>
			</div>
		</div>
	</div>
</body>
</html>