<%@page import="model.UserService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<%
String id = request.getParameter("ID");
if(true){
	Map<String,String> u = new UserService().getUserById(id);
}
%>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<!-- 优化后的用户页面，用户id通过管理员管理获取获取 -->
<body>
<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-info">用户注册</h5>
				</div>
				<form method="post" action="../UpdataUser?ID=<%=id%>">
					<div class="modal-body">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">用户名</label>
							<div class="col-sm-10">
								<input class="form-control" id="username" name="un" valuetype="text"
									required /> <span class="text-danger" id="checkInfo"></span>
							</div>

						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">密码</label>
							<div class="col-sm-10">
								<input class="form-control" name="pw" type="password" required />
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">电话</label>
							<div class="col-sm-10">
								<input class="form-control" name="tel" type="number" required />
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">地址</label>
							<div class="col-sm-10">
								<input class="form-control" name="addr" type="text" required />
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary">确定</button>
					</div>
				</form>
			</div>
		</div>
</body>
</html>